package ch.swissbytes.syscomappprintexample

import ch.swissbytes.syscomappprint.activities.PrinterBaseActivity
import ch.swissbytes.syscomappprint.utils.SysPrinterInterface


class MainActivity : PrinterBaseActivity() {

    override fun onStartPrinting(printer: SysPrinterInterface) {
        printer.printDashedLine()
        printer.printCenteredBold("Foo bar")
        printer.printCenteredBold("Foo bar")
        printer.printDashedLine()
        printer.printLn("Foo bar baz new test")
        printer.printLn("Foo bar baz new test")
        printer.print()
    }
}

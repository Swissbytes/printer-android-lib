package ch.swissbytes.syscomappprint.printers

import android.bluetooth.BluetoothDevice
import android.graphics.Bitmap
import android.util.Log
import ch.swissbytes.syscomappbase.extensions.toFormattedString
import ch.swissbytes.syscomappbase.listeners.GenericListener
import ch.swissbytes.syscomappbase.listeners.SimpleListener
import ch.swissbytes.syscomappprint.extensions.doAsync
import ch.swissbytes.syscomappprint.utils.GuaranteeLine
import ch.swissbytes.syscomappprint.utils.ProdDeliveryNoteLine
import ch.swissbytes.syscomappprint.utils.ProdLine
import ch.swissbytes.syscomappprint.utils.SysPrinterInterface
import ch.swissbytes.syscomappprint.zebra.kotlin.ZebraFontFactory
import ch.swissbytes.syscomappprint.zebra.kotlin.ZebraFontType
import ch.swissbytes.syscomappprint.zebra.kotlin.ZebraPrinterKt
import com.zebra.sdk.comm.ConnectionException
import com.zebra.sdk.printer.ZebraPrinterLanguageUnknownException
import kotlin.math.ceil
import kotlin.math.min

class ZebraPrinterImpl(override val device: BluetoothDevice) : SysPrinterInterface {

    private val TAG = this.javaClass.simpleName

    lateinit var zebraPrinter: ZebraPrinterKt
    private val pattern: String? = null

    override val isConnected: Boolean get() = zebraPrinter.isConnected

    override fun connect(onConnectionDone: GenericListener<Boolean>, onFail: SimpleListener) {
        doAsync {
            try {
                connect(
                    ZebraPrinterKt(
                        device.address
                    )
                )
                onConnectionDone.invoke(zebraPrinter.isConnected)
            } catch (e: java.lang.Exception) {
                Log.e(TAG, "printer error ${e.message}", e)
                onFail.invoke()
            }
        }
    }

    @Throws(ConnectionException::class)
    private fun connect(printer: ZebraPrinterKt) {
        zebraPrinter = printer
        zebraPrinter.open()
        zebraPrinter.setMediaFeedLength(0)
    }

    override fun printLn(vararg values: String) {
        values.forEach { zebraPrinter.writeAndGoToNextLine(it) }
    }

    override fun writeRightJustify(value: String) {
        val format = "%48s"
        zebraPrinter.write(String.format(format, value))
    }

    override fun write(value: String) {
        zebraPrinter.setX(8.toDouble())
        zebraPrinter.write(value)
    }

    override fun printProdLines(values: List<ProdLine>) {
        val maxPatternTot = 10
        val maxPatternPName = 21
        val maxWidth = maxPatternPName.toDouble()
        val pattern = " %-5s%${maxPatternPName}s %8s %${maxPatternTot}s"
        printLn(String.format(pattern, "Cant.", "Producto", "P.Unit.", "SubTot Bs"))
        printDashedLine()
        values.forEach { prod ->
            val pName = prod.name
            val nameLength= pName.length
            val numOfLines = if (pName.isEmpty()) 0 else ceil(pName.length / maxWidth).toInt() - 1
            (0..numOfLines).forEach { curLine ->
                val firstLine = curLine == 0
                val from = if (firstLine) 0 else curLine * maxWidth.toInt()
                val to = from + min(maxWidth.toInt() - 1, (nameLength -1) - from)
                printLn(
                    if (firstLine) String.format(
                        pattern,
                        prod.qty.toString(),
                        pName.substring(from..to),
                        "${prod.unitPrice.toFormattedString()} ",
                        prod.totalPrice.toFormattedString()
                    )
                    else String.format(pattern, "", pName.substring(from..to), "", "")
                )
            }
        }
        printDashedLine()
    }

    override fun printDeliveryNoteProdLines(values: List<ProdDeliveryNoteLine>) {
        val maxPatternQty = 5
        val maxPatternUnitMeasure = 7
        val maxPatternPName = 32
        val maxWidth = maxPatternPName.toDouble()
        val pattern = " %-${maxPatternQty}s %-${maxPatternUnitMeasure}s %-${maxPatternPName}s"
        printBoldLn(String.format(pattern, "CANT.", "UM", "DESCRIPCION"))
        printDashedLine()
        values.forEach { prod ->
            val pName = prod.name
            val nameLength= pName.length
            val numOfLines = if (pName.isEmpty()) 0 else ceil(pName.length / maxWidth).toInt() - 1
            (0..numOfLines).forEach { curLine ->
                val firstLine = curLine == 0
                val from = if (firstLine) 0 else curLine * maxWidth.toInt()
                val to = from + min(maxWidth.toInt() - 1, (nameLength -1) - from)
                val sub = pName.substring(from..to)
                val choppedName = if(sub.isBlank()) "." else sub
                printLn(
                    if (firstLine) String.format(
                        pattern,
                        prod.qty.toString(),
                        prod.unitMeasure.toString(),
                        choppedName)
                    else String.format(pattern, "", "", choppedName)
                )
            }
        }
        printDashedLine()
    }

    override fun printGuaranteeLines(prods: List<GuaranteeLine>) {
        val maxPatternTot = 19
        val maxPatternPName = 25
        val maxWidth = maxPatternPName.toDouble()
        val pattern = " %-${maxPatternPName}s %${maxPatternTot}s"
        printLn(String.format(pattern, " Productos", "Dots"))
        printDashedLine()

        prods.forEachIndexed { i, prod ->
            if (i != 0) printCentered("---------")
            val details = prod.detail
            val pName = prod.pName
            val nameLength= pName.length
            val numOfLines = if (pName.isEmpty()) 0 else ceil(pName.length / maxWidth).toInt() - 1
            (0..numOfLines).forEach { curLine ->
                val firstLine = curLine == 0
                val from = if (firstLine) 0 else curLine * maxWidth.toInt()
                val to = from + min(maxWidth.toInt() - 1, (nameLength -1) - from)
                printLn(String.format(pattern, pName.substring(from..to), if (firstLine) details[0] else ""))
            }
            val to = details.size - 1
            (1..to).forEach { j ->
                val value = details[j]
                Log.i(TAG, "print value: $value")
                printLn(String.format(pattern, "", value))
            }
        }
        printDashedLine()
    }

    override fun print() {
        try {
            zebraPrinter.print()
        } catch (e: Exception) {
        }
    }

    override fun printBoldLn(value: String) {
        zebraPrinter.setBold(2)
        printLn(value)
        setNormalMode()
    }

    override fun leftAndRight(left: String, right: String) {
        val width = 46
        val margin = width - left.length - 1
        val value = String.format("%s %" + margin + "s", left, right)
        printLn(value)
    }

    override fun printImage(image: Bitmap) {
        try {
            zebraPrinter.printImage(image, 140, 0, 300, 300)
        } catch (e: ConnectionException) {
        } catch (e: ZebraPrinterLanguageUnknownException) {
        }
    }

    override fun printLnRightJustify(value: String) {
        val format = "%48s"
        zebraPrinter.writeAndGoToNextLine(String.format(format, value))
    }

    override fun start() {}

    override fun stop() {}

    override fun printCentered(value: String) {
        val maxWidth = 38.toDouble()
        val words = value.split(" ".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
        var currLineLength = 0
        var sb = StringBuilder()
        words.forEach { s ->
            currLineLength += s.length + 1 // 1 is the white space
            if (currLineLength > maxWidth){
                currLineLength = 0
                zebraPrinter.writeAndGoToNextLine(sb.toString(), true)
                sb = StringBuilder()
            }
            sb.append(s)
            sb.append(" ")
        }
        zebraPrinter.writeAndGoToNextLine(sb.toString(), true)

    }

    override fun printCenteredBold(value: String) {
        zebraPrinter.setBold(2)
        printCentered(value)
        setNormalMode()
    }

    override fun addLine(value: String) {
        zebraPrinter.goToTheNextLine()
    }

    override fun printTitle(value: String) {
        zebraPrinter.writeAndGoToNextLine(value, true)
        setNormalMode()
        addLine()
    }

    private fun setNormalMode() {
        zebraPrinter.font = ZebraFontFactory.getZebraFont(ZebraFontType.CONTENT_FONT)
        zebraPrinter.setBold(0)
    }

    override fun printSignature(image: Bitmap?, vararg values: String) {
        try {
            if (image != null) {
                zebraPrinter.printImage(image, 50, 0, 400, image.height / 2)
            } else {
                addSpace()
            }
            for (i in values.indices) {
                addLine()
                printCentered(values[i])
            }
        } catch (e: ConnectionException) {
            Log.e(TAG, "printImage", e)
        } catch (e: ZebraPrinterLanguageUnknownException) {
            Log.e(TAG, "printImage", e)
        }

        print()
    }

    override fun addLine() {
        zebraPrinter.goToTheNextLine()
    }

    private fun addSpace(space: Int = 1) {
        for (i in 0..space) {
            addLine()
        }
    }

    override fun setAlignModeCenter() {

    }

    val TAG_BOLD = "<bold>"
    val TAG_CENTER = "<center>"
    override fun printHeaderLine(input: String) {
        var value = input

        val bold = value.contains(TAG_BOLD)
        val center = value.contains(TAG_CENTER)

        if (bold) value = value.replace(TAG_BOLD, "")
        if (center) value = value.replace(TAG_CENTER, "")

        if(bold && center) {
            printCenteredBold(value)
        } else if (bold) {
            printBoldLn(value)
        } else if (center){
            printCentered(value)
        } else {
            printLn(value)
        }
    }

    override fun printLine() {
        zebraPrinter.writeAndGoToNextLine("__________________________________________", true)
    }

    override fun printDashedLine() {
        zebraPrinter.writeAndGoToNextLine("------------------------------------------", true)
    }
}
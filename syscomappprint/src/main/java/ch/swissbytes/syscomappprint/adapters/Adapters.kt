package ch.swissbytes.syscomappprint.adapters

import android.bluetooth.BluetoothClass
import android.bluetooth.BluetoothDevice
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import ch.swissbytes.syscomappbase.listeners.GenericListener
import ch.swissbytes.syscomappprint.activities.adapter.BluetoothDeviceMultiAdapter


@BindingAdapter("bluetoothDeviceAdapter", "adapterListener")
fun bluetoothDeviceAdapter(rv: RecyclerView, devices: MutableList<BluetoothDevice>, listener: GenericListener<BluetoothDevice>) {
    rv.adapter = BluetoothDeviceMultiAdapter(listener, devices.splitInPrinterSections())
}

@BindingAdapter("onRefreshListener")
fun onRefreshListener(refreshLayout: SwipeRefreshLayout, listener: SwipeRefreshLayout.OnRefreshListener?) {
    refreshLayout.setOnRefreshListener(listener)
}

@BindingAdapter("refreshing")
fun onStopRefresh(refreshLayout: SwipeRefreshLayout, refreshing: Boolean) {
    if (!refreshing && refreshLayout.isRefreshing) {
        refreshLayout.isRefreshing = false
    }
}

fun MutableList<BluetoothDevice>.splitInPrinterSections(): MutableList<Any> {
    val printers: MutableList<Any> = this.filter { it.isPrinter() }.toMutableList()
    val otherDevices: MutableList<Any>  = this.filter { !it.isPrinter() }.toMutableList()
    if (!printers.isEmpty()) printers.add(0, PrinterSection(true))
    if (!otherDevices.isEmpty()) otherDevices.add(0, PrinterSection(false))
    printers.addAll(otherDevices)
    return printers
}

data class PrinterSection(var isPrinter: Boolean)

//http://bluetooth-pentest.narod.ru/software/bluetooth_class_of_device-service_generator.html
fun BluetoothDevice.isPrinter(): Boolean =
    this.bluetoothClass.hasService(BluetoothClass.Service.RENDER) && this.bluetoothClass.majorDeviceClass == BluetoothClass.Device.Major.IMAGING




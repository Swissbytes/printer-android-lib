package ch.swissbytes.syscomappprint.activities

import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import ch.swissbytes.syscomappbase.extensions.showToast
import ch.swissbytes.syscomappprint.R
import ch.swissbytes.syscomappprint.activities.adapter.BluetoothDeviceAdapter
import ch.swissbytes.syscomappprint.utils.BluetoothPrinterService
import ch.swissbytes.syscomappprint.utils.DeviceWrapper
import ch.swissbytes.syscomappprint.utils.SysPrinterInterface
import ch.swissbytes.syscomappprint.utils.getPrinterImplementation
import kotlinx.android.synthetic.main.activity_print_base.*


abstract class PrinterBaseActivity : AppCompatActivity() {

    private lateinit var bluetoothPrinterService: BluetoothPrinterService
    private val adapter by lazy { rv.adapter as BluetoothDeviceAdapter }
    var printer: SysPrinterInterface? = null
    val textViewContent by lazy { content!! }
    var deviceSelectedAndConnected: DeviceWrapper? = null
            set(value) {
                field = value
                val enable = value != null
                if (!enable) printer = null

                runOnUiThread {
                    with(print_btn){
                        alpha = if (enable) 1f else .3f
                        isEnabled = enable
                    }
                }
            }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_print_base)
//        toolbar = binding.root.toolbar
        setUpActionBar()
        setUpDeviceRv()

        print_btn.setOnClickListener {
            printer?.let {
                onStartPrinting(it)
            }
        }
        scan.setOnClickListener { bluetoothPrinterService.startDiscovery() }

        bluetoothPrinterService = BluetoothPrinterService(this,
            onBluetoothNotSupported = { showToast(getString(R.string.bluetooth_not_supported)) },
            onDefaultDeviceFound = { adapter.getDevice(it)?.let { device -> tryToConnectDevice(device) } },
            onDeviceBondStateChange = { adapter.deviceStateChange(it) },
            onActionDiscoveryStart = { progress.visibility = View.VISIBLE },
            onActionDiscoveryStop = { progress.visibility = View.GONE },
            onBluetoothTurnedOff = {
                showEmptyStateList(true)
                deviceSelectedAndConnected = null
                adapter.eraseAll()
            },
            onNewPrinterFound = {
                showEmptyStateList(false)
                adapter.addDevice(it)
            }
        )
    }

    private fun showEmptyStateList(show: Boolean){
        if (!show && emptyState.visibility == View.VISIBLE){
            emptyState.visibility = View.GONE
            rv.visibility = View.VISIBLE
        } else if (show && emptyState.visibility == View.GONE){
            emptyState.visibility = View.VISIBLE
            rv.visibility = View.GONE
        }
    }

    private fun setUpActionBar(){
        setSupportActionBar(toolbar)
        actionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when(item?.itemId){
            android.R.id.home -> this.finish()
        }
        return super.onOptionsItemSelected(item)
    }

    private fun setUpDeviceRv(){
        rv?.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = BluetoothDeviceAdapter(mutableListOf(), onDeviceClickListener = { onDeviceCLick(it)})
            addItemDecoration(DividerItemDecoration(context, LinearLayoutManager.VERTICAL))
        }
    }

    override fun onResume() {
        super.onResume()
        adapter.reorderDevices()
    }

    private fun onDeviceCLick(deviceWrapper: DeviceWrapper){
        bluetoothPrinterService.stopDiscovery()
        if (!deviceWrapper.isBond()){
            deviceWrapper.device.createBond()
        }
        tryToConnectDevice(deviceWrapper)
    }

    private fun tryToConnectDevice(deviceWrapper: DeviceWrapper){
        printer = deviceWrapper.device
            .getPrinterImplementation().apply {
                connect(
                    onConnectionDone = {
                        showToast(getString(R.string.connection_done))
                        deviceWrapper.isConnected = true
                        deviceWrapper.isSelected = true
                        deviceSelectedAndConnected = deviceWrapper
                        bluetoothPrinterService.prefs.defaultDevice = deviceWrapper.device.address
                        runOnUiThread { adapter.onDeviceConnect(deviceWrapper) }
                    })
                {
                    showToast(getString(R.string.connection_fail))
                    deviceWrapper.isInConnectionProcess = false
                    runOnUiThread { adapter.deviceStateChange(deviceWrapper) }

                    if (bluetoothPrinterService.prefs.defaultDevice.isEmpty()){
                        bluetoothPrinterService.prefs.defaultDevice = deviceWrapper.device.address
                    }
                }
            }
    }

    override fun onDestroy() {
        super.onDestroy()
        bluetoothPrinterService.onDestroy()
    }

    abstract fun onStartPrinting(printer: SysPrinterInterface)
}

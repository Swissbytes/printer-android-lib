package ch.swissbytes.syscomappprint.activities.adapter

import android.bluetooth.BluetoothDevice
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import ch.swissbytes.syscomappbase.listeners.GenericListener
import ch.swissbytes.syscomappprint.R
import ch.swissbytes.syscomappprint.adapters.PrinterSection


class BluetoothDeviceMultiAdapter(private val listener: GenericListener<BluetoothDevice>, private val devices: MutableList<Any>)
    : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private lateinit var context: Context
    private val HEADER: Int = 0
    private val DEVICES: Int = 1

    override fun getItemViewType(position: Int): Int =
        when (devices[position]){
            is PrinterSection -> HEADER
            is BluetoothDevice -> DEVICES
            else -> DEVICES
        }

    override fun getItemCount(): Int = devices.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        context = parent.context;
        return when (viewType){
            HEADER -> {
                val itemView = LayoutInflater.from(context).inflate(R.layout.list_item_printer, parent, false)
                BluetoothDeviceMultiAdapter.VhHeader(itemView) }
            DEVICES -> {
                val itemView = LayoutInflater.from(context).inflate(R.layout.list_item_two_lines, parent, false)
                BluetoothDeviceMultiAdapter.VhDevice(itemView) }
            else -> BluetoothDeviceMultiAdapter.VhHeader(View(context))
        }
    }

    override fun onBindViewHolder(v: RecyclerView.ViewHolder, position: Int) {
        val device = devices[position]
        when(device){
            is BluetoothDevice -> {
                val vh = v as VhDevice
                val _name = device.name
                val name = if (_name != null && !_name.isEmpty()) _name else context.getString(R.string.unknown)
                vh.primary.text = name
                vh.secondary.text = device.address
                v.itemView.setOnClickListener { listener.invoke(device) }
            }
            is PrinterSection -> {
                val vh = v as VhHeader
                vh.primary.text =  context.getString(if (device.isPrinter) R.string.printer else R.string.other_devices)
            }
        }
    }

    class VhDevice(v: View) : RecyclerView.ViewHolder(v) {
        var primary = v.findViewById<TextView>(R.id.text1)
        var secondary =  v.findViewById<TextView>(R.id.text2)
    }

    class VhHeader(v: View) : RecyclerView.ViewHolder(v) {
        var primary = v.findViewById<TextView>(android.R.id.text1)
    }

}

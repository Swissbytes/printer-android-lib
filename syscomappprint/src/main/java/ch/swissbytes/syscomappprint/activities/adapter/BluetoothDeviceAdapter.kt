package ch.swissbytes.syscomappprint.activities.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import ch.swissbytes.syscomappbase.listeners.GenericListener
import ch.swissbytes.syscomappprint.R
import ch.swissbytes.syscomappprint.utils.DeviceWrapper


class BluetoothDeviceAdapter(
    private var list: MutableList<DeviceWrapper>,
    private val onDeviceClickListener: GenericListener<DeviceWrapper>
) : RecyclerView.Adapter<BluetoothDeviceAdapter.ViewHolder>() {


    override fun getItemCount(): Int = list.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder =
        ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.list_item_printer, parent, false))


    override fun onBindViewHolder(holder: ViewHolder, p: Int) {
        val position = holder.layoutPosition
        val device = list[position]
        holder.primary.text = device.device.name
        holder.secondary.text = device.device.address
        with(holder.paired) { visibility = if (device.isBond()) View.VISIBLE else  View.GONE }
        with(holder.img) { visibility = if (device.isAbleToPrint()) View.VISIBLE else  View.GONE }
        holder.itemView.setOnClickListener {
            onDeviceClickListener.invoke(device)
        }
    }


    @Synchronized fun addDevice(deviceWrapper: DeviceWrapper){
        if (list.firstOrNull { it.device.address == deviceWrapper.device.address } == null){
            list.add(deviceWrapper)
            reorderDevices()
        }
    }

    @Synchronized fun deviceStateChange(deviceWrapper: DeviceWrapper){
        forDevice(deviceWrapper) { position ->
            notifyItemRangeChanged(position, position+1)
        }
    }

    private fun forDevice(deviceWrapper: DeviceWrapper, listener: GenericListener<Int>){
        val (position, _) = findDevicePositionByMacAddress(deviceWrapper.device.address)
        if (position != -1){
            listener.invoke(position)
        }
    }

    @Synchronized fun onDeviceConnect(deviceWrapper: DeviceWrapper){
        list.forEach { if (deviceWrapper.device.address != it.device.address) it.isSelected = false }
        reorderDevices()
    }

    @Synchronized fun reorderDevices(){
        list.sortWith(compareBy({ it.isSelected }, { it.isConnected }, { it.isBond() }))
        list.reverse()
        notifyDataSetChanged()
    }

    private fun findDevicePositionByMacAddress(deviceAddress: String): Pair<Int, DeviceWrapper?>{
        list.forEachIndexed { index, deviceWrapper ->
            if (deviceWrapper.device.address == deviceAddress){
                return Pair(index, deviceWrapper)
            }
        }
        return Pair(-1, null)
    }

    fun getDevice(macAddress: String): DeviceWrapper? {
        val (_, device) = findDevicePositionByMacAddress(macAddress)
        return device
    }

    fun eraseAll() {
        list.clear()
        notifyDataSetChanged()
    }


    class ViewHolder(v: View) : RecyclerView.ViewHolder(v) {
        val primary = v.findViewById<TextView>(R.id.text1)
        val secondary = v.findViewById<TextView>(R.id.text2)
        val paired = v.findViewById<View>(R.id.paired)
        val img = v.findViewById<View>(R.id.img)
    }

}
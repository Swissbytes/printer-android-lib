package ch.swissbytes.syscomappprint.zebra.kotlin

class HeaderTitleFont: ZebraFont {
    
    override var number: Int = 0

    override var size: Int = 6

    override var width: Double = 4.0

    override var height: Double = 36.0
    
}

package ch.swissbytes.syscomappprint.zebra.kotlin

class ContentFont : ZebraFont {
    
    override var number: Int = 7

    override var size: Int = 0

    override var width: Double = 1.5

    override var height: Double = 24.0
    
}
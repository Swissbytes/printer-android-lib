package ch.swissbytes.syscomappprint.zebra.kotlin

object ZebraFontFactory {
    fun getZebraFont(fontType: ZebraFontType?): ZebraFont {
        return when (fontType) {
            ZebraFontType.HEADER_TITLE_FONT -> HeaderTitleFont()
            ZebraFontType.TITLE_FONT -> TitleFont()
            ZebraFontType.CONTENT_FONT -> ContentFont()
            else -> throw IllegalArgumentException("font type not exists")
        }
    }
}
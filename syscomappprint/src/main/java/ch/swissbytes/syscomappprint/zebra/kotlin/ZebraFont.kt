package ch.swissbytes.syscomappprint.zebra.kotlin

interface ZebraFont {
    /**
     * Get the number of the font E.g. 0 or 7
     *
     * @return int
     */
    val number: Int

    /**
     * Get the size of the font
     *
     * @return int
     */
    val size: Int

    /**
     * Get the width of the font in dots. 8 dots = 1 mm
     *
     * @return with in dots
     */
    val width: Double

    /**
     * Get the height of the font in dots. 8 dots = 1 mm
     *
     * @return height in dots
     */
    val height: Double
}
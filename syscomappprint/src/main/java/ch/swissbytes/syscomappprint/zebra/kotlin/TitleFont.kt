package ch.swissbytes.syscomappprint.zebra.kotlin

class TitleFont: ZebraFont {
    
    override var number: Int = 0

    override var size: Int = 3

    override var width: Double = 2.0

    override var height: Double = 18.0
    
}

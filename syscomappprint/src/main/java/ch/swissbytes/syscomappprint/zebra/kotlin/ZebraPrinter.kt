package ch.swissbytes.syscomappprint.zebra.kotlin

import android.graphics.Bitmap
import android.os.Looper
import com.zebra.sdk.comm.BluetoothConnection
import com.zebra.sdk.comm.Connection
import com.zebra.sdk.comm.ConnectionException
import com.zebra.sdk.graphics.internal.ZebraImageAndroid
import com.zebra.sdk.printer.PrinterLanguage
import com.zebra.sdk.printer.ZebraPrinterFactory
import com.zebra.sdk.printer.ZebraPrinterLanguageUnknownException
import java.io.UnsupportedEncodingException

class ZebraPrinterKt {
    private var builder: StringBuilder
    var macAddress: String
    private var bluetoothConnection: Connection? = null

    /**
     * Contains functions to interact with the printer
     *
     * @param macAddress Mac address of the printer
     */
    constructor(macAddress: String) {
        builder = StringBuilder()
        font = ZebraFontFactory.getZebraFont(ZebraFontType.CONTENT_FONT)
        this.macAddress = macAddress
    }

    /**
     * Contains functions to interact with the printer
     *
     * @param macAddress Mac address of the printer
     * @param font       ZebraFont instance for printing
     */
    constructor(macAddress: String, font: ZebraFont) {
        builder = StringBuilder()
        this.font = font
        this.macAddress = macAddress
    }

    var font: ZebraFont
        set(value) {
            field = value
            builder.append("! U1 SETLP ")
                .append(value.number)
                .append(" ")
                .append(value.size)
                .append(" ")
                .append(value.height)
                .append("\r\n")
        }

    fun printQrCode(value: String) {
        try {
            bluetoothConnection!!.write((value + "\r\n").toByteArray())
        } catch (e: ConnectionException) {
        }
    }

    /**
     * Change the position of the print head to the specified x position
     *
     * @param x Value in dots. 8 dots = 1mm
     */
    fun setX(x: Double) {
        builder.append("! U1 X ")
            .append(x)
            .append("\r\n")
    }

    /**
     * Change the position of the print head to the specified y position
     *
     * @param y Value in dots. 8 dots = 1mm
     */
    fun setY(y: Double) {
        builder.append("! U1 Y ")
            .append(y)
            .append("\r\n")
    }

    /**
     * Change the position of the print head to the specified x and y position
     *
     * @param x Value in dots. 8 dots = 1mm
     * @param y Value in dots. 8 dots = 1mm
     */
    fun setXY(x: Double, y: Double) {
        builder.append("! U1 XY ")
            .append(x)
            .append(" ")
            .append(y)
            .append("\r\n")
    }

    /**
     * Change the position of the print head relative to his current position in x
     *
     * @param x Value in dots. 8 dots = 1mm
     */
    fun setRX(x: Double) {
        builder.append("! U1 RX ")
            .append(x)
            .append("\r\n")
    }

    /**
     * Change the position of the print head relative to his current position in y
     *
     * @param y Value in dots. 8 dots = 1mm
     */
    fun setRY(y: Double) {
        builder.append("! U1 RY ")
            .append(y)
            .append("\r\n")
    }

    /**
     * Change the position of the print head relative to his current position in x and y
     *
     * @param x Value in dots. 8 dots = 1mm
     * @param y Value in dots. 8 dots = 1mm
     */
    fun setRXY(x: Double, y: Double) {
        builder.append("! U1 RX ")
            .append(x)
            .append(" ")
            .append(y)
            .append("\r\n")
    }

    /**
     * Set Bold to the current font.
     *
     * @param offset value between 0 and 5 that indicates how Bold must be the text. 0 removes Bold from the font
     */
    fun setBold(offset: Int) {
        builder.append("! U1 SETBOLD ")
            .append(offset)
            .append("\r\n")
    }

    /**
     * Write the specified text in the current position of the print head
     *
     * @param text Text to write
     */
    fun write(text: String?) {
        builder.append(text)
    }

    /**
     * Write the specified text and then move the print head to the next line
     *
     * @param text Text to write
     */
    fun writeAndGoToNextLine(text: String?) {
        builder.append(text)
            .append(" \r\n")
    }

    /**
     * Write the specified text center in the page and then move the print head to the next line.
     *
     * @param text   Text to write
     * @param center True if the text must be center or False if not
     */
    fun writeAndGoToNextLine(text: String, center: Boolean) {
        if (center) {
            center(text)
        }
        writeAndGoToNextLine(text)
    }

    /**
     * Move the print head to the next line of the page
     */
    fun goToTheNextLine() {
        builder.append("\r\n")
    }

    /**
     * Center a text in the page
     *
     * @param text Text to center
     */
    private fun center(text: String) {
        val textLength: Double = text.length * font.width
        val posX =
            ((PAGE_WIDTH - PAGE_MARGIN) / 2 - textLength / 2) * DPM
        setRX(posX)
    }

    /**
     * Creates and open a connection to the printer
     *
     * @throws ConnectionException
     */
    @Throws(ConnectionException::class)
    fun open() {
        try {
            Looper.prepare()
        } catch (e: Exception) {
            Looper.myLooper().quit()
        }
        bluetoothConnection = BluetoothConnection(macAddress).apply {
            maxTimeoutForRead = 5000
            open()
        }

    }

    /**
     * Close the connection with the printer
     *
     * @throws ConnectionException
     */
    @Throws(ConnectionException::class)
    fun close() {
        if (isConnected) {
            bluetoothConnection!!.close()
        }
        //        Looper.myLooper().quit();
    }

    val isConnected: Boolean
        get() = bluetoothConnection != null && bluetoothConnection!!.isConnected

    /**
     * Send the data to print to the printer
     *
     * @throws ConnectionException
     * @throws InterruptedException
     * @throws UnsupportedEncodingException
     */
    @Throws(
        ConnectionException::class,
        InterruptedException::class,
        UnsupportedEncodingException::class
    )
    fun print() {
        val lpData = builder.toString()
        bluetoothConnection!!.write(lpData.toByteArray(charset("ISO-8859-1")))
        Thread.sleep(500)
        builder.setLength(0)
    }

    /**
     * Set the feed length after printing
     *
     * @param value Value in dots. 8 dots = 1 mm
     */
    fun setMediaFeedLength(value: Int) {
        builder.append("! U1 setvar \"media.feed_length\" ")
            .append("\"")
            .append(value)
            .append("\"")
            .append("\r\n")
    }

    /**
     * Prints an image as monochrome image
     *
     * @param image  the image to be printed
     * @param x      horizontal starting position in dots
     * @param y      vertical starting position in dots
     * @param width  desired width of the printed image. Passing a value less than 1 will preserve original width.
     * @param height desired height of the printed image. Passing a value less than 1 will preserve original width.
     * @throws ConnectionException
     * @throws ZebraPrinterLanguageUnknownException
     */
    @Throws(
        ConnectionException::class,
        ZebraPrinterLanguageUnknownException::class
    )
    fun printImage(image: Bitmap?, x: Int, y: Int, width: Int, height: Int) {
        val zebraPrinter =
            ZebraPrinterFactory.getInstance(
                PrinterLanguage.CPCL,
                bluetoothConnection
            )
        zebraPrinter.printImage(ZebraImageAndroid(image), x, y, width, height, false)
    }

    /**
     * Prints a line
     *
     * @param x0    X-coordinate of the top-left corner
     * @param y0    Y-coordinate of the top-left corner
     * @param x1    X-coordinate of:
     *
     *  - top right corner for horizontal
     *
     *  - bottom left corner for vertical
     * @param y1    Y-coordinate of:
     *
     *  - top right corner for horizontal
     *
     *  - bottom left corner for vertical
     * @param width Unit-width (or thickness) of the line
     */
    fun printLine(x0: Int, y0: Int, x1: Int, y1: Int, width: Int) {
        builder.append("! 0 200 200 10 1")
            .append("\r\n")
            .append("LINE ").append(x0).append(" ").append(y0).append(" ").append(x1).append(" ")
            .append(y1).append(" ").append(width)
            .append("\r\n")
            .append("FORM")
            .append("\r\n")
            .append("PRINT")
            .append("\r\n")
    }

    companion object {
        /**
         * Size of the printer page in mm
         */
        const val PAGE_WIDTH = 76.2
        /**
         * Dots per mm
         */
        const val DPM = 8.0
        /**
         * Width of the page margin in mm
         */
        const val PAGE_MARGIN = 4.4
    }
}

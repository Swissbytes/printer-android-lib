package ch.swissbytes.syscomappprint.zebra.kotlin

enum class ZebraFontType {
    HEADER_TITLE_FONT, TITLE_FONT, CONTENT_FONT
}
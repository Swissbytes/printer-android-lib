package ch.swissbytes.syscomappprint.pref

import android.content.Context
import android.content.SharedPreferences
import android.preference.PreferenceManager


class PrintPrefs(context: Context) {

    private val DEFAULT_DEVICE = "default_bluetooth_device"
    private val SHOW_ADVICE = "show_advice"

    val sharedPreferences: SharedPreferences by lazy { PreferenceManager.getDefaultSharedPreferences(context) }

    var defaultDevice: String
        get() = this.sharedPreferences.getString(DEFAULT_DEVICE, "")
        set(v) = this.sharedPreferences.edit().putString(DEFAULT_DEVICE, v).apply()

    var showAdvice: Boolean
        get() = this.sharedPreferences.getBoolean(SHOW_ADVICE, true)
        set(v) = this.sharedPreferences.edit().putBoolean(SHOW_ADVICE, v).apply()

    companion object {
        private var mPreferences: PrintPrefs? = null
        fun getInstance(context: Context): PrintPrefs {
            if (mPreferences == null) mPreferences = PrintPrefs(context)
            return mPreferences!!
        }
    }

}
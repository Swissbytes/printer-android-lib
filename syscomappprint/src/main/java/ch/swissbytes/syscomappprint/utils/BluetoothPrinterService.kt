package ch.swissbytes.syscomappprint.utils

import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothAdapter.*
import android.bluetooth.BluetoothDevice
import android.bluetooth.BluetoothDevice.ACTION_BOND_STATE_CHANGED
import android.bluetooth.BluetoothDevice.EXTRA_DEVICE
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.util.Log
import ch.swissbytes.syscomappbase.extensions.showToast
import ch.swissbytes.syscomappbase.listeners.GenericListener
import ch.swissbytes.syscomappbase.listeners.SimpleListener
import ch.swissbytes.syscomappprint.R
import ch.swissbytes.syscomappprint.adapters.isPrinter
import ch.swissbytes.syscomappprint.pref.PrintPrefs


data class DeviceWrapper(
    val device: BluetoothDevice,
    var isSelected: Boolean = false,
    var isConnected: Boolean = false,
    var isInConnectionProcess: Boolean = false,
    val isInBondingProcess: Boolean = false
) {
    fun isBond() = device.isBond()
    fun isAbleToPrint() = isSelected && isConnected
}

class BluetoothPrinterService(private val context: Context,
                              val prefs: PrintPrefs = PrintPrefs(context),
                              private val onBluetoothNotSupported: SimpleListener? = null,
                              val onNewPrinterFound: GenericListener<DeviceWrapper>? = null,
                              val onDefaultDeviceFound: GenericListener<String>? = null,
                              val onDeviceBondStateChange: GenericListener<DeviceWrapper>? = null,
                              val onActionDiscoveryStart: SimpleListener? = null,
                              val onActionDiscoveryStop: SimpleListener? = null,
                              val onBluetoothTurnedOff: SimpleListener? = null
) {

    private val TAG = this::class.java.simpleName
    val bluetoothAdapter: BluetoothAdapter? by lazy { getDefaultAdapter() }

    private val broadcastReceiver by lazy {
        object : BroadcastReceiver() {
            override fun onReceive(context: Context?, intent: Intent?) {
                when(intent?.action){
                    BluetoothDevice.ACTION_FOUND -> { onDeviceFound(intent.getParcelableExtra(EXTRA_DEVICE))}
                    ACTION_DISCOVERY_STARTED -> {
                        context?.apply { showToast(getString(R.string.searching_devices)) }
                        onActionDiscoveryStart?.invoke()
                    }
                    ACTION_DISCOVERY_FINISHED -> {
                        Log.i(TAG, "ACTION_DISCOVERY_FINISHED")
                        onActionDiscoveryStop?.invoke()
                    }
                    ACTION_STATE_CHANGED -> {
                        when (intent.getIntExtra(EXTRA_STATE, -1)){
                            STATE_OFF -> {
                                Log.i(TAG, "ACTION_STATE_CHANGED STATE_OFF")
                                onBluetoothTurnedOff?.invoke()
                            }
                            STATE_ON -> {
                                Log.i(TAG, "ACTION_STATE_CHANGED STATE_ON")
                                retrieveBondedDevicesAndStartScanning()
                            }
                        }
                    }
                    ACTION_CONNECTION_STATE_CHANGED -> {
                        when (intent.getIntExtra(EXTRA_CONNECTION_STATE , -1)){
                            STATE_DISCONNECTING -> Log.i(TAG, "ACTION_CONNECTION_STATE_CHANGED STATE_OFF")
                            STATE_CONNECTED -> Log.i(TAG, "ACTION_CONNECTION_STATE_CHANGED STATE_OFF")
                            STATE_DISCONNECTED -> Log.i(TAG, "ACTION_CONNECTION_STATE_CHANGED STATE_OFF")
                            STATE_CONNECTING -> Log.i(TAG, "ACTION_CONNECTION_STATE_CHANGED STATE_OFF")

                        }
                    }
                    ACTION_BOND_STATE_CHANGED -> {
                        val device  = intent.getParcelableExtra<BluetoothDevice>(EXTRA_DEVICE)
                        onDeviceBondStateChange?.invoke(DeviceWrapper(device))
                    }
                }
            }
        }
    }

    private fun setBroadcastReceiver(){
        val receiver = IntentFilter()
        receiver.addAction(BluetoothDevice.ACTION_FOUND)
        receiver.addAction(ACTION_DISCOVERY_FINISHED)
        receiver.addAction(ACTION_DISCOVERY_STARTED)
        receiver.addAction(ACTION_STATE_CHANGED)
        receiver.addAction(ACTION_BOND_STATE_CHANGED)
        context.registerReceiver(broadcastReceiver, receiver)
    }


    /**
     * We boot up the bluetooth state change listener
     * and request the user permission for enabling bluetooth if OFF
     */
    init {
        setBroadcastReceiver()
        if (bluetoothAdapter == null){
            onBluetoothNotSupported?.invoke()
        } else {
            if (!bluetoothAdapter!!.isEnabled) {
                bluetoothAdapter?.enable()
            } else {
                retrieveBondedDevicesAndStartScanning()
            }
        }

    }

    fun startDiscovery(){
        if (bluetoothAdapter != null) {
            bluetoothAdapter?.startDiscovery()
        } else {
            onBluetoothNotSupported?.invoke()
        }
    }

    private fun retrieveBondedDevicesAndStartScanning(){
        val pairedDevices: Set<BluetoothDevice>? = bluetoothAdapter?.bondedDevices
        var defaultDeviceFound = false
        pairedDevices?.forEach { device ->
            //Check if device is in preferences
            onDeviceFound(device, isPaired = true)

            val deviceAddress = device.address
            if (prefs.defaultDevice == deviceAddress) {
                onDefaultDeviceFound?.invoke(deviceAddress)
                defaultDeviceFound = true
            }
        }
        if (!defaultDeviceFound){
            bluetoothAdapter?.startDiscovery()
        }
    }

    @Synchronized
    private fun onDeviceFound(device: BluetoothDevice, isPaired: Boolean = false){
        if (device.isPrinter()){
            onNewPrinterFound?.invoke(DeviceWrapper(device))
        }
    }


    fun onDestroy(){
        bluetoothAdapter?.cancelDiscovery()
        context.unregisterReceiver(broadcastReceiver)
    }

    fun stopDiscovery() {
        bluetoothAdapter?.cancelDiscovery()
    }
}
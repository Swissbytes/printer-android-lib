package ch.swissbytes.syscomappprint.utils

import android.bluetooth.BluetoothDevice
import android.bluetooth.BluetoothDevice.*
import android.graphics.Bitmap
import ch.swissbytes.syscomappbase.listeners.GenericListener
import ch.swissbytes.syscomappbase.listeners.SimpleListener
import ch.swissbytes.syscomappprint.printers.ZebraPrinterImpl

enum class SysPrinterType {
    ZEBRA
}

data class ProdLine(val qty: Int, val name: String, val unitPrice: String, val totalPrice: String)
data class GuaranteeLine(val pName: String, val detail: List<String>)
data class ProdDeliveryNoteLine(val qty: Int, val name: String, val unitMeasure: String? = null)
//data class GuaranteeDetail(val ref: String, val price: String)

interface SysPrinterInterface {
    val device: BluetoothDevice
    val isConnected: Boolean
    fun connect(onConnectionDone: GenericListener<Boolean>, onFail: SimpleListener)
    fun printLn(vararg values: String)
    fun leftAndRight(left: String, right: String)
    fun printBoldLn(value: String)
    fun printImage(qrCode: Bitmap)
    fun printLnRightJustify(value: String)
    fun write(value: String)
    fun printProdLines(values: List<ProdLine>)
    fun printDeliveryNoteProdLines(values: List<ProdDeliveryNoteLine>)
    fun printGuaranteeLines(prods: List<GuaranteeLine>)
    fun writeRightJustify(value: String)
    fun print()
    fun start()
    fun stop()
    fun printCentered(value: String)
    fun printCenteredBold(value: String)
    fun addLine(value: String)
    fun addLine()
    fun printTitle(value: String)
    fun printSignature(image: Bitmap?, vararg values: String)
    fun setAlignModeCenter()
    fun printDashedLine()
    fun printLine()
    fun printHeaderLine(input: String)
}

fun BluetoothDevice.getPrinterImplementation(type: SysPrinterType = SysPrinterType.ZEBRA) =
    when (type) {
        SysPrinterType.ZEBRA -> {
            ZebraPrinterImpl(this)
        }
    }

fun BluetoothDevice.isBond() = this.bondState == BOND_BONDED
fun BluetoothDevice.isBonding() = this.bondState == BOND_BONDING
fun BluetoothDevice.isUnBond() = this.bondState == BOND_NONE

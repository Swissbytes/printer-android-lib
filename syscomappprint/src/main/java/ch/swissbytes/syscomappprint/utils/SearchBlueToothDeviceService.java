package ch.swissbytes.syscomappprint.utils;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import ch.swissbytes.syscomappprint.pref.PrintPrefs;

import java.util.Set;


public class SearchBlueToothDeviceService {

    private Context context;
    private PrintPrefs mPrefs;
    private OnDeviceFound onDeviceFoundListener;
    private OnDeviceFound defaultDeviceFoundListener;
    private BlueToothListener noBluetoothListener;
    private BlueToothListener bluetoothFoundListener;
    private BlueToothListener onDiscoveryStart;
    private BlueToothListener onDiscoveryStop;

    private final BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

    public interface OnDeviceFound {
        void onDeviceFound(BluetoothDevice device);
    }

    public interface BlueToothListener{
        void call();
    }

    public SearchBlueToothDeviceService(Context context) {
        this.context = context;
        this.mPrefs = PrintPrefs.Companion.getInstance(context);
        setUpBroadCastReceivers();
    }

    private void setUpBroadCastReceivers(){
        IntentFilter receiver = new IntentFilter();
        receiver.addAction(BluetoothDevice.ACTION_FOUND);
        receiver.addAction(BluetoothAdapter.ACTION_DISCOVERY_FINISHED);
        receiver.addAction(BluetoothAdapter.ACTION_DISCOVERY_STARTED);
        receiver.addAction(BluetoothAdapter.ACTION_STATE_CHANGED);
        context.registerReceiver(broadcastReceiver, receiver);
    }

    public void onDestroy() {
        context.unregisterReceiver(broadcastReceiver);
    }

    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action != null) {
                switch (action){
                    case BluetoothDevice.ACTION_FOUND: onNewDeviceFound(intent);
                        break;
                    case BluetoothAdapter.ACTION_DISCOVERY_FINISHED:  onActionDiscoveryStop();
                        break;
                    case BluetoothAdapter.ACTION_DISCOVERY_STARTED: onActionDiscoveryStarted();
                        break;
                    case BluetoothAdapter.ACTION_STATE_CHANGED:  onBlueToothStatusChange(intent);
                }
            }
        }
    };

    private void onNewDeviceFound(Intent intent){
        BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
        if (device != null) {
            addDevice(device);
        }
    }

    private void onActionDiscoveryStop(){
        if (onDiscoveryStop != null) {
            onDiscoveryStop.call();
        }
    }

    private void onActionDiscoveryStarted(){
        if (onDiscoveryStart != null) {
            onDiscoveryStart.call();
        }
    }

    private void onBlueToothStatusChange(Intent intent){
        int status = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, -1);
        if (status == BluetoothAdapter.STATE_OFF) {
            if (noBluetoothListener != null) {
                noBluetoothListener.call();
            }
        }

        if (status == BluetoothAdapter.STATE_ON) {
            if (bluetoothFoundListener != null) {
                bluetoothFoundListener.call();
            }
            scanDeviceAndNotifyIfDefaultFound();
        }
    }

    public SearchBlueToothDeviceService setOnDiscoveryStart(BlueToothListener listener){
        this.onDiscoveryStart = listener;
        return this;
    }

    public SearchBlueToothDeviceService setOnDiscoveryStop(BlueToothListener listener){
        this.onDiscoveryStop = listener;
        return this;
    }

    public SearchBlueToothDeviceService setOnDeviceFoundListener(OnDeviceFound listener){
        this.onDeviceFoundListener = listener;
        return this;
    }

    public SearchBlueToothDeviceService setOnDefaultDeviceFoundListener(OnDeviceFound listener){
        this.defaultDeviceFoundListener = listener;
        return this;
    }

    public SearchBlueToothDeviceService setNoBluetoothListener(BlueToothListener noBluetoothListener) {
        this.noBluetoothListener = noBluetoothListener;
        return this;
    }

    public SearchBlueToothDeviceService setBluetoothFoundListener(BlueToothListener bluetoothFoundListener) {
        this.bluetoothFoundListener = bluetoothFoundListener;
        return this;
    }

    public synchronized SearchBlueToothDeviceService scanDeviceAndNotifyIfDefaultFound(){
        if (mBluetoothAdapter != null) {
            Set<BluetoothDevice> pairedDevices = mBluetoothAdapter.getBondedDevices();
            if (pairedDevices.size() > 0) {
                for (BluetoothDevice device : pairedDevices) {
                    addDevice(device);
                    if (defaultDeviceFoundListener != null && mPrefs.getDefaultDevice().equals(device.getAddress())){
                        defaultDeviceFoundListener.onDeviceFound(device);
                    }
                }
            }
        }
        return this;
    }

    public void addDevice(BluetoothDevice device){
        if (onDeviceFoundListener != null) {
            onDeviceFoundListener.onDeviceFound(device);
        }
    }

    public boolean starDiscovering(){
        boolean startDiscovering = false;
        if (mBluetoothAdapter != null) {
            if (!mBluetoothAdapter.isDiscovering()) {
                mBluetoothAdapter.startDiscovery();
                startDiscovering = true;
            }
        }
        return startDiscovering;
    }
}

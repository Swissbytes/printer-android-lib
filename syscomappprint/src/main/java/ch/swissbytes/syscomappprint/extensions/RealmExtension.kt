package ch.swissbytes.syscomappprint.extensions

import io.realm.Realm
import io.realm.RealmModel
import io.realm.RealmQuery

fun <T: RealmModel> realmGetItemCopy(myClass: Class<T>, realm: Realm = Realm.getDefaultInstance(), f: (RealmQuery<T?>.() -> RealmQuery<T?>)? = null):
        T? = realm.use {
    val item = if (f != null) f(it.where(myClass)).findFirst() else it.where(myClass).findFirst()
    if (item != null) it.copyFromRealm(item) else null
}

fun realmPersist(model: Any, realm: Realm = Realm.getDefaultInstance())
        = when(model){
    is RealmModel -> realmTransaction(realm) { copyToRealmOrUpdate(model) }
    is List<*> -> realmTransaction(realm) { model.checkRealmModelList()?.let { copyToRealmOrUpdate(it) } }
    else -> { throw IllegalArgumentException("Only realm object and list can be passed to realmPersist") }
}

fun realmUse(realm: Realm = Realm.getDefaultInstance(), f: Realm.() -> Unit) = realm.use { f(it) }

fun realmTransaction(realm: Realm = Realm.getDefaultInstance(), f: Realm.() -> Unit)
        = realmUse(realm) { executeTransaction { r -> f(r) } }

/**
 * @return null if the list is empty
 */
@Suppress("UNCHECKED_CAST")
fun List<*>.checkRealmModelList() = if (all { it is RealmModel }) this as List<RealmModel> else null

